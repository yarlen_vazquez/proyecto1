const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");
const port = process.env.PORT || 3000

const app = express();
app.set("view engine", "ejs")
app.use(express.static(__dirname + "/public"));
app.use(bodyparser.urlencoded({extended:true}));
app.get("/", (req, res) => {
    // res.send("Iniciando servidor 3000 para un nuevo servidor web");
    //declarar un array de objetos

    let datos = [{
            matricula: "2019030399",
            nombre: "ACOSTA ORTEGA HUMBERTO",
            sexo: "M",
            materias: ["ingles", "base de datos", "tecnologias Internet"]
        },
        {
            matricula: "2020030310",
            nombre: "ACOSTA VARELA IRVING GUADALUPE",
            sexo: "M",
            materias: ["ingles", "base de datos", "tecnologias Internet"]

        },
        {
            matricula: "202003007",
            nombre: "ALMOGABAR VAZQUEZ YARLEN DE JESUS",
            sexo: "F",
            materias: ["ingles", "base de datos", "tecnologias Internet"]
        },

    ]
    res.render('index', {
        titulo: "Mi primer pagina en Embedded JavaScript",
        Nombre: "Almogabar Yarlen",
        Grupo: "8-3",
        listado: datos
    })
})
app.get("/tabla", (req, res) => {
    const params = {
        numero: req.query.numero
    }
    res.render('tabla',params);
})

app.post("/tabla", (req, res) => {
    const params = {
        numero: req.body.numero
    }
    res.render('tabla',params);
})

app.get('/cotizacion',(req,res)=>{
    const params ={
        valor:req.query.valor,
        pinicial:req.query.pinicial,
        plazo:req.query.plazo
    }
    res.render('cotizacion',params);

})

app.post('/cotizacion',(req,res)=>{
    const params ={
        valor:req.body.valor,
        pinicial:req.body.pinicial,
        plazo:req.body.plazo
    }
    res.render('cotizacion',params);
})

//la pagina del error va al final de los get/post
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/public/error.html')
})
const puerto = 3000;
app.listen(puerto, () => {
    console.log("Iniciando puerto");

});